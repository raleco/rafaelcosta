unit unPrincipal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, Data.Win.ADODB, Vcl.DBCtrls,
  Vcl.Grids, Vcl.DBGrids, Vcl.StdCtrls, Vcl.ExtCtrls;

type
  TAcao = (acInserir, acEditar);

  TfPrincipal = class(TForm)
    pnMenu: TPanel;
    btnExcluir: TButton;
    btnEditar: TButton;
    btnCadastrar: TButton;
    DBGrid1: TDBGrid;
    btnFechar: TButton;
    DBNavigator1: TDBNavigator;
    procedure btnCadastrarClick(Sender: TObject);
    procedure btnEditarClick(Sender: TObject);
    procedure btnExcluirClick(Sender: TObject);
    procedure btnFecharClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure Cadastro(Acao: TAcao);
  end;

var
  fPrincipal: TfPrincipal;

implementation

{$R *.dfm}

uses unCadProduto, unTeste;

procedure TfPrincipal.Cadastro(Acao: TAcao);
begin
  if Acao = acInserir then
    dmTeste.tbProduto.Append
  else if Acao = acEditar then
    dmTeste.tbProduto.Edit;
  Application.CreateForm(TfCadProduto, fCadProduto);
  fCadProduto.ShowModal;
  if fCadProduto.ModalResult = mrOK then
    dmTeste.tbProduto.Post
  else
    dmTeste.tbProduto.Cancel;
  fCadProduto.Free;
end;

procedure TfPrincipal.btnCadastrarClick(Sender: TObject);
begin
  Cadastro(acInserir);
end;

procedure TfPrincipal.btnEditarClick(Sender: TObject);
begin
  if not dmTeste.tbProduto.IsEmpty then
  Cadastro(acEditar);
end;

procedure TfPrincipal.btnExcluirClick(Sender: TObject);
begin
  if not dmTeste.tbProduto.IsEmpty then
  begin
    if (MessageDlg('Deseja excluir o registro selecionado?', mtConfirmation, [mbYes, mbNo], 0) = mrYes) then
      dmTeste.tbProduto.Delete;
  end;
end;

procedure TfPrincipal.btnFecharClick(Sender: TObject);
begin
  Close;
end;

end.
