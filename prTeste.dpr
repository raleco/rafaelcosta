program prTeste;

uses
  Vcl.Forms,
  unCadProduto in 'unCadProduto.pas' {fCadProduto},
  unPrincipal in 'unPrincipal.pas' {fPrincipal},
  unTeste in 'unTeste.pas' {dmTeste: TDataModule};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TdmTeste, dmTeste);
  Application.CreateForm(TfPrincipal, fPrincipal);
  Application.Run;
end.
