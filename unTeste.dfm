object dmTeste: TdmTeste
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 146
  Width = 282
  object dsProduto: TDataSource
    DataSet = tbProduto
    Left = 176
    Top = 32
  end
  object tbProduto: TADOTable
    Connection = conTeste
    CursorType = ctStatic
    BeforePost = tbProdutoBeforePost
    TableName = 'PRODUTO'
    Left = 112
    Top = 32
    object tbProdutoID_PRODUTO: TAutoIncField
      DisplayLabel = 'ID do Produto'
      DisplayWidth = 14
      FieldName = 'ID_PRODUTO'
      ReadOnly = True
      DisplayFormat = '000000'
    end
    object tbProdutoDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      DisplayWidth = 62
      FieldName = 'DESCRICAO'
      Size = 128
    end
    object tbProdutoPRECO: TBCDField
      DisplayLabel = 'Pre'#231'o'
      DisplayWidth = 13
      FieldName = 'PRECO'
      DisplayFormat = ',0.00'
      Precision = 18
      Size = 2
    end
    object tbProdutoESTOQUE: TBCDField
      DisplayLabel = 'Estoque'
      DisplayWidth = 11
      FieldName = 'ESTOQUE'
      DisplayFormat = ',0.0000'
      Precision = 18
    end
    object tbProdutoUNIDADE: TStringField
      DisplayLabel = 'UN'
      DisplayWidth = 3
      FieldName = 'UNIDADE'
      Size = 2
    end
    object tbProdutoDATA_CADASTRO: TDateTimeField
      Alignment = taCenter
      DisplayLabel = 'Data de Cadastro'
      DisplayWidth = 22
      FieldName = 'DATA_CADASTRO'
      DisplayFormat = 'dd/mm/yyyy'
    end
  end
  object conTeste: TADOConnection
    ConnectionString = 
      'Provider=SQLOLEDB.1;Password=sa;Persist Security Info=True;User ' +
      'ID=sa;Initial Catalog=TESTE;Data Source=.\sqlexpress'
    LoginPrompt = False
    Provider = 'SQLOLEDB.1'
    Left = 48
    Top = 32
  end
end
