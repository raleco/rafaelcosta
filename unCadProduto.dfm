object fCadProduto: TfCadProduto
  Left = 0
  Top = 0
  Caption = 'Cadastro de Produto'
  ClientHeight = 217
  ClientWidth = 445
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  PixelsPerInch = 96
  TextHeight = 13
  object gbPrincipal: TGroupBox
    Left = 0
    Top = 0
    Width = 445
    Height = 182
    Align = alClient
    Caption = 'Informa'#231#245'es Principais'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 0
    object Label1: TLabel
      Left = 65
      Top = 39
      Width = 16
      Height = 13
      Alignment = taRightJustify
      Caption = 'ID:'
    end
    object Label2: TLabel
      Left = 23
      Top = 66
      Width = 58
      Height = 13
      Alignment = taRightJustify
      Caption = 'Descri'#231#227'o:'
    end
    object Label3: TLabel
      Left = 46
      Top = 93
      Width = 35
      Height = 13
      Alignment = taRightJustify
      Caption = 'Pre'#231'o:'
    end
    object Label4: TLabel
      Left = 33
      Top = 120
      Width = 48
      Height = 13
      Alignment = taRightJustify
      Caption = 'Estoque:'
    end
    object Label5: TLabel
      Left = 32
      Top = 147
      Width = 49
      Height = 13
      Alignment = taRightJustify
      Caption = 'Unidade:'
    end
    object DBEdit1: TDBEdit
      Left = 86
      Top = 31
      Width = 65
      Height = 21
      CharCase = ecUpperCase
      DataField = 'ID_PRODUTO'
      DataSource = dmTeste.dsProduto
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
    end
    object DBEdit2: TDBEdit
      Left = 86
      Top = 58
      Width = 347
      Height = 21
      CharCase = ecUpperCase
      DataField = 'DESCRICAO'
      DataSource = dmTeste.dsProduto
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
    end
    object DBEdit3: TDBEdit
      Left = 86
      Top = 85
      Width = 65
      Height = 21
      CharCase = ecUpperCase
      DataField = 'PRECO'
      DataSource = dmTeste.dsProduto
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
    end
    object DBEdit4: TDBEdit
      Left = 86
      Top = 112
      Width = 65
      Height = 21
      CharCase = ecUpperCase
      DataField = 'ESTOQUE'
      DataSource = dmTeste.dsProduto
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 3
    end
    object DBComboBox1: TDBComboBox
      Left = 86
      Top = 139
      Width = 65
      Height = 21
      Style = csDropDownList
      CharCase = ecUpperCase
      DataField = 'UNIDADE'
      DataSource = dmTeste.dsProduto
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      Items.Strings = (
        'UN'
        'PC'
        'KG'
        'CX'
        'LT')
      ParentFont = False
      TabOrder = 4
    end
  end
  object pnBottom: TPanel
    Left = 0
    Top = 182
    Width = 445
    Height = 35
    Align = alBottom
    TabOrder = 1
    object Button1: TButton
      Left = 369
      Top = 1
      Width = 75
      Height = 33
      Align = alRight
      Caption = 'Cancelar'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ModalResult = 2
      ParentFont = False
      TabOrder = 0
    end
    object Button2: TButton
      Left = 1
      Top = 1
      Width = 75
      Height = 33
      Align = alLeft
      Caption = 'Gravar'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ModalResult = 1
      ParentFont = False
      TabOrder = 1
    end
  end
end
