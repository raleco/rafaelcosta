unit unTeste;

interface

uses
  System.SysUtils, System.Classes, Data.DB, Data.Win.ADODB, Forms;

type
  TdmTeste = class(TDataModule)
    dsProduto: TDataSource;
    tbProduto: TADOTable;
    tbProdutoID_PRODUTO: TAutoIncField;
    tbProdutoDESCRICAO: TStringField;
    tbProdutoPRECO: TBCDField;
    tbProdutoESTOQUE: TBCDField;
    tbProdutoUNIDADE: TStringField;
    tbProdutoDATA_CADASTRO: TDateTimeField;
    conTeste: TADOConnection;
    procedure tbProdutoBeforePost(DataSet: TDataSet);
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dmTeste: TdmTeste;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

procedure TdmTeste.DataModuleCreate(Sender: TObject);
begin
  conTeste.Connected := False;
  conTeste.ConnectionString := PromptDataSource(0, 'Provider=SQLOLEDB.1;Password=sa;Persist Security Info=True;User ID=sa;Initial Catalog=TESTE;Data Source=.\sqlexpress');
  if conTeste.ConnectionString <> '' then
  begin
    conTeste.Connected := True;
  end;
  if conTeste.Connected then
  tbProduto.Open;
end;

procedure TdmTeste.tbProdutoBeforePost(DataSet: TDataSet);
begin
  if DataSet.State = dsInsert then
  tbProdutoDATA_CADASTRO.AsDateTime := Date;
end;

end.
